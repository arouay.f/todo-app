import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireAuthModule} from '@angular/fire/auth';

/*export const firebaseConfig = {
  apiKey: 'AIzaSyDjSNyUGXZ2hbpngPjFFBv2V6AO94DeZak',
  authDomain: 'todo-app-1ab80.firebaseapp.com',
  projectId: 'todo-app-1ab80',
  storageBucket: 'todo-app-1ab80.appspot.com',
  messagingSenderId: '647965295238',
  appId: '1:647965295238:web:e0ad8ddbc796f79c344707'
};*/
export const firebaseConfig = {
  apiKey: 'AIzaSyDfdcHDvMmVsqqYg3zUmpXv3ZuvU6kTmFM',
  authDomain: 'todo-9a2a0.firebaseapp.com',
  projectId: 'todo-9a2a0',
  storageBucket: 'todo-9a2a0.appspot.com',
  messagingSenderId: '892849197883',
  appId: '1:892849197883:web:7c62bd292fafc9bf495d1e'
};
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, AngularFireModule.initializeApp(firebaseConfig),
  AngularFireDatabaseModule,
  AngularFireAuthModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
