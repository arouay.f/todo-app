import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AuthenticationService} from '../services/authentication.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  currentDate: string;
  newTask: string;
  allTasks = [];
  user: any;
  add = false;
  constructor(private angFire: AngularFireDatabase, private authService: AuthenticationService, private navCtrl: NavController) {
    const todayDate = new Date();
    const options = {weekday :  'long', month : 'long', day : 'numeric'};
    this.currentDate = todayDate.toLocaleDateString('en-en', options);
  }

  addNewTask() {
    this.angFire.list('Tasks/').push({
      text : this.newTask,
      checked : false,
      date : new Date().toISOString(),
      user : this.user
    });
    JSON.parse( JSON.stringify({
      text : this.newTask,
      checked : false,
      date : new Date().toISOString(),
      user : this.user
    }));
    this.newTask = '';
  }

  ngOnInit(): void {
    this.authService.userDetails().subscribe(res => {
      console.log('res', res);
      if (res !== null) {
        this.user = res.uid;
        console.log(this.user);
      } else {
        this.navCtrl.navigateBack('');
      }
    }, err => {
      console.log('err', err);
    });
    this.getTasks();
  }

  private getTasks() {
    this.angFire.list('/Tasks').snapshotChanges(['child_added']).subscribe(
        (response) => {
          console.log(response);
          this.allTasks = [];
          response.forEach(element => {
            // console.log(element.payload.exportVal());
            this.allTasks.push({
              key : element.key,
              text : element.payload.exportVal().text,
              checked : element.payload.exportVal().checked,
              date : element.payload.exportVal().date.substring(11, 16),
              user: element.payload.exportVal().user
            });
            this.allTasks = this.allTasks.filter(t => t.user === this.user);
            console.log(this.allTasks);
          });
        }
    );
  }
  changCheckedState(tsk) {
    this.angFire.object(`Tasks/${tsk.key}/checked`).set(tsk.checked);
  }

  logout() {
    this.authService.logoutUser()
        .then(res => {
          console.log(res);
          this.user = '';
          this.allTasks = [];
          this.navCtrl.navigateForward('login');
        }).catch(error => {
          console.log(error);
    });
  }

}
