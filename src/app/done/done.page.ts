import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AuthenticationService} from '../services/authentication.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-done',
  templateUrl: './done.page.html',
  styleUrls: ['./done.page.scss'],
})
export class DonePage implements OnInit {
  currentDate: string;
  allTasks = [];
  user: any;
  constructor(private angFire: AngularFireDatabase, private authService: AuthenticationService, private navCtrl: NavController) {
    const todayDate = new Date();
    const options = {weekday :  'long', month : 'long', day : 'numeric'};
    this.currentDate = todayDate.toLocaleDateString('en-en', options);
  }

  ngOnInit() {
    this.authService.userDetails().subscribe(res => {
      console.log('res', res);
      if (res !== null) {
        this.user = res.uid;
        console.log(this.user);
      } else {
        this.navCtrl.navigateBack('');
      }
    }, err => {
      console.log('err', err);
    });
    this.getTasks();
  }

  private getTasks() {
    this.angFire.list('/Tasks').snapshotChanges(['child_added']).subscribe(
        (response) => {
          console.log(response);
          this.allTasks = [];
          response.forEach(element => {
            // console.log(element.payload.exportVal());
            this.allTasks.push({
              key : element.key,
              text : element.payload.exportVal().text,
              checked : element.payload.exportVal().checked,
              date : element.payload.exportVal().date.substring(11, 16),
              user: element.payload.exportVal().user
            });
            this.allTasks = this.allTasks.filter(t => t.user === this.user && t.checked === true);
            console.log(this.allTasks);
          });
        }
    );
  }
  logout() {
    this.authService.logoutUser()
        .then(res => {
          console.log(res);
          this.user = '';
          this.allTasks = [];
          this.navCtrl.navigateForward('login');
        }).catch(error => {
      console.log(error);
    });
  }


}
