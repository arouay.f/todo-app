import { Component, OnInit } from '@angular/core';
import {AlertController} from '@ionic/angular';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  constructor(private alertCtrl: AlertController, private authService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {
  }

  async reset(form: NgForm) {
    console.log(form.value.email);
    if (form.valid) {
      this.authService.reset(form.value.email).then(() => this.router.navigate(['login']))
    } else {
      const alert = await this.alertCtrl.create({
        header: 'email not valid',
        buttons: ['OK']
      })
      await alert.present();
    }

  }
}
