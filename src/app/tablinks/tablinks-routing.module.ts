import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TablinksPage } from './tablinks.page';
import {TodoPage} from '../todo/todo.page';

const routes: Routes = [
  {
    path: 'tablinks',
    component: TablinksPage,
    children: [{
      path: 'home',
      loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
    },
      {
        path: 'done',
        loadChildren: () => import('../done/done.module').then(m => m.DonePageModule)
      },
      {
        path: '',
        // redirectTo: 'tablinks',
        // pathMatch: 'full'
        component: TodoPage
      }]
  },
  {
    path: '',
    redirectTo: 'tablinks',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TablinksPageRoutingModule {}
