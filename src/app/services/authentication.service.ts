import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  // userTasks = [];
  constructor(private angFire: AngularFireDatabase, private auth: AngularFireAuth) { }

  registerUser(user) {
    return new Promise<any>((resolve, reject) => {
      this.auth.createUserWithEmailAndPassword(user.email, user.password)
          .then(res => resolve(res),
              err => reject(err));
    });
  }

  loginUser(user) {
    return new Promise<any>((resolve, reject) => {
      this.auth.signInWithEmailAndPassword(user.email, user.password).then(
          res => {resolve(res); localStorage.setItem('token', 'token')},
          err => reject(err)
      );
    });
  }

  logoutUser() {
    return new Promise((resolve, reject) => {
      if (this.auth.currentUser) {
        this.auth.signOut().then(() => {
          console.log('LOG OUT');
          resolve();
          localStorage.clear();
        }).catch((error) => {
          reject();
        });
      }
    });
  }

  userDetails() {
    return this.auth.user;
  }
  reset(email) {
    return this.auth.sendPasswordResetEmail(email);
  }
  /*getTasks() {
    this.angFire.list('/Tasks').snapshotChanges(['child_added']).subscribe(
        (response) => {
          console.log(response);
          this.userTasks = [];
          response.forEach(element => {
            // console.log(element.payload.exportVal());
            this.userTasks.push({
              key: element.key,
              text: element.payload.exportVal().text,
              checked: element.payload.exportVal().checked,
              date: element.payload.exportVal().date.substring(11, 16),
              user: element.payload.exportVal().user
            });
            // this.userTasks = this.userTasks.filter(t => t.user === this.auth.user.uid);
            console.log(this.userTasks);
          });
        }
    );
    // return this.userTasks;
  }*/
}

